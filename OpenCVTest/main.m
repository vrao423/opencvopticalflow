//
//  main.m
//  OpenCVTest
//
//  Created by Venkat Rao on 8/8/16.
//  Copyright © 2016 Rao Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

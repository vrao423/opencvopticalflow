//
//  AppDelegate.h
//  OpenCVTest
//
//  Created by Venkat Rao on 8/8/16.
//  Copyright © 2016 Rao Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  ViewController.m
//  OpenCVTest
//
//  Created by Venkat Rao on 8/8/16.
//  Copyright © 2016 Rao Studios. All rights reserved.
//

#import "ViewController.h"
#import <opencv2/highgui/cap_ios.h>

using namespace cv;

@interface ViewController ()<CvVideoCameraDelegate> {
    
    cv::vector<cv::Point2f>     _points[2];
    cv::vector<cv::Point2f>     _originalPoints;
    cv::Size                    _winSize;
    cv::Size                    _subPixWinSize;
    cv::TermCriteria            _termcrit;
    bool                        _addRemovePt;
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *buttonStart;
@property (weak, nonatomic) IBOutlet UISlider *slider;

@end

@implementation ViewController

static const NSUInteger MAX_COUNT = 500;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:self.imageView];
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPresetLow;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    
    self.videoCamera.delegate = self;
    
    _winSize = cv::Size(31,31);
    _subPixWinSize = cv::Size(20,20);

    _termcrit = cv::TermCriteria(cv::TermCriteria::COUNT|cv::TermCriteria::EPS,20,0.03);
}

#ifdef __cplusplus

bool addRemovePt = false;
Point2f point;
bool needToInit = false;
Mat mask;
cv::vector<Scalar> colors;
cv::Mat                     _currentFrame;
cv::Mat                     _previousFrame;

- (void)processImage:(Mat&)image {
    Mat image_copy;
    
    Mat edges;
    image.copyTo(edges);
    blurEdgeQuick(edges, self.slider.value);
    tr_opticalFlow(image, _winSize, _subPixWinSize, _termcrit, _points, _originalPoints, self.slider.value);
    draw(image, _points, 0.8);
}

void warp(cv::vector<cv::Point2f> original,
          cv::vector<cv::Point2f> affine,
          Mat &img1Cropped,
          Mat &img2Cropped) {
    
//    NSLog(@"points: %d", points[0].type());
    
    Mat map_x, map_y;

    map_x.create(img1Cropped.size(), CV_32FC1);
    map_y.create(img1Cropped.size(), CV_32FC1);
    
    for( int j = 0; j < img1Cropped.rows; j++ ) {
        for( int i = 0; i < img1Cropped.cols; i++ ) {
            map_x.at<float>(j, i) = i;
            map_y.at<float>(j, i) = j;
        }
    }
    
    for( int i = 0; i < affine.size(); i++) {
        Point2f point1 = affine[i];
        if(point1.x > 0 && point1.y > 0) {
            map_x.at<float>(affine[i].y, affine[i].x) = original[i].x;
            map_y.at<float>(affine[i].y, affine[i].x) = original[i].y;
        }
    }
    
    remap(img1Cropped, img2Cropped, map_x, map_y, INTER_NEAREST);
}

void blurEdgeQuick(Mat &image, float value) {
        Mat blurImage;
        Mat edges;
    
        cv::blur(image, edges, cv::Size(3,3));
        cv::Canny(edges, edges, value, value * 3.0);
    
        Mat dst;
        dst.create(image.size(), image.type());
        dst = Scalar::all(0);
        image.copyTo(dst, edges);
    
        dst.copyTo(image);
}

void tr_opticalFlow(Mat &image,
                    cv::Size                    winSize,
                    cv::Size                    subPixWinSize,
                    cv::TermCriteria            termcrit,
                    cv::vector<cv::Point2f>     points[2],
                    cv::vector<cv::Point2f>     originalPoints,
                    float value) {
    
    // start optical flow
    if (_currentFrame.empty()) {
        cvtColor(image, _currentFrame, COLOR_BGR2GRAY);
        needToInit = true;
    }
    
    _currentFrame.copyTo(_previousFrame);
    cvtColor(image, _currentFrame, COLOR_BGR2GRAY);
    
    if(needToInit) {
        // automatic initialization
        goodFeaturesToTrack(_currentFrame, points[1], MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
        cornerSubPix(_currentFrame, points[1], subPixWinSize, cv::Size(-1,-1), termcrit);
        mask.release();
        mask.create(image.size(), image.type());
        size_t i;
        for(i = 0; i < points[1].size(); i++) {
            colors.push_back(Scalar(rand() % 255, rand() % 255, rand() % 255));
        }
        
        originalPoints = points[1];
        
        needToInit = false;
    } else if(!points[0].empty()) {
        Mat output;
        vector<uchar> status;
        vector<float> err;
        
        calcOpticalFlowPyrLK(_previousFrame, _currentFrame, points[0], points[1], status, err, winSize, value, termcrit, 0, 0.001);
        
        //        NSLog(@"old: %lu new: %lu", _points[0].size(), _points[1].size());
        
        
        
        
        //        cv::Point2f newPoint1;
        //        cv::Point2f newPoint2;
        //        //add corners
        //        for (i = 0; i < _currentFrame.cols; i = i + 25) {
        //             newPoint1 = cv::Point2f(i,0);
        //             newPoint2 = cv::Point2f(i,_currentFrame.rows - 1);
        //            if ([self canAddPoints:_points[1] point:newPoint1]) {
        //                _points[1].push_back(newPoint1);
        //            }
        //
        //            if ([self canAddPoints:_points[1] point:newPoint2]) {
        //                _points[1].push_back(newPoint2);
        //            }
        //
        //        }
        //
        //        for (k = 0; k < _currentFrame.rows; k = k + 25) {
        //            newPoint2 = cv::Point2f(0,k);
        //            newPoint1 = cv::Point2f(_currentFrame.cols - 1, k);
        //
        //            if ([self canAddPoints:_points[1] point:newPoint1]) {
        //                _points[1].push_back(newPoint1);
        //            }
        //
        //            if ([self canAddPoints:_points[1] point:newPoint2]) {
        //                _points[1].push_back(newPoint2);
        //            }
        //        }
        warp(points[0], points[1], image, image);
    }
    
    std::swap(points[1], points[0]);
}

void draw(Mat &image,
          cv::vector<cv::Point2f> points[],
          float threshold) {
    
    size_t i;
    for(i = 0; i < points[1].size(); i++) {
        Scalar color(255.0, 0,0);
        if (pow(pow((points[1][i].x - points[0][i].x), 2.0) + pow((points[1][i].y - points[0][i].y), 2.0), 0.5) > threshold) {
            color = Scalar(0.0, 255.0, 0.0);
        }
        circle(image, points[1][i], 3, color, -1, 1);
        cv::line(mask, points[1][i], points[0][i], colors[i]);
    }
    
//    cv::add(image, mask, image);
}

-(BOOL)canAddPoints:(cv::vector<cv::Point2f>)points point:(cv::Point2f)point {
    size_t i;
    for(i = 0; i < points.size(); i++) {
        if (points[i].x == point.x && points[i].y == point.y) {
            return NO;
        }
    }
    return YES;
}

#endif

- (IBAction)actionStart:(id)sender {
    [self.videoCamera start];
    needToInit = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

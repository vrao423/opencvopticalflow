//
//  ViewController.h
//  OpenCVTest
//
//  Created by Venkat Rao on 8/8/16.
//  Copyright © 2016 Rao Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <opencv2/highgui/cap_ios.h>
using namespace cv;

@interface ViewController : UIViewController

@property (nonatomic, strong) CvVideoCamera *videoCamera;


@end

